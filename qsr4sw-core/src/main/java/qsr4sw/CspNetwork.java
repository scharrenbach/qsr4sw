package qsr4sw;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 
 * <p>
 * This class holds the network for a constraint satisfaction problem.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 * @version 0.0.2
 * @since 0.0.2
 */
public class CspNetwork {

	private final List<CspConstraint> _constraintsList;

	private int _maxNode;

	//
	//
	//

	public CspNetwork() {
		_constraintsList = new ArrayList<CspConstraint>();
	}

	public CspNetwork(List<CspConstraint> constraints) {
		this();
		this.addAll(constraints);
	}

	//
	//
	//

	private void updateMaxNode() {
		_maxNode = -1;
		for (CspConstraint constraint : _constraintsList) {
			_maxNode = Math.max(_maxNode, constraint.getLeftOp());
			_maxNode = Math.max(_maxNode, constraint.getRightOp());
		}
	}

	//
	//
	//

	public CspNetwork add(CspConstraint constraint) {
		_constraintsList.add(constraint);
		updateMaxNode();
		return this;
	}

	public CspNetwork addAll(Collection<CspConstraint> constraints) {
		_constraintsList.addAll(constraints);
		updateMaxNode();
		return this;
	}

	public CspNetwork remove(CspConstraint constraint) {
		_constraintsList.remove(constraint);
		updateMaxNode();
		return this;
	}

	public CspNetwork removeAll(Collection<CspConstraint> constraints) {
		_constraintsList.removeAll(constraints);
		updateMaxNode();
		return this;
	}

	public List<CspConstraint> getConstraints() {
		return new ArrayList<CspConstraint>(_constraintsList);
	}

	public int getMaxNode() {
		return _maxNode;
	}

}
