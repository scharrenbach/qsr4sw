package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import static qsr4sw.GeoSPARQL.NS_GEO;
import static qsr4sw.GeoSPARQL.rcc8dc;
import static qsr4sw.GeoSPARQL.rcc8ec;
import static qsr4sw.GeoSPARQL.rcc8eq;
import static qsr4sw.GeoSPARQL.rcc8ntpp;
import static qsr4sw.GeoSPARQL.rcc8ntppi;
import static qsr4sw.GeoSPARQL.rcc8po;
import static qsr4sw.GeoSPARQL.rcc8tpp;
import static qsr4sw.GeoSPARQL.rcc8tppi;
import qsr4sw.GeoSPARQL;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;

/**
 * <p>
 * Jena resources for the {@link GeoSPARQL} vocabulary.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.1
 * @version 0.0.1
 * 
 */
public final class GeoSPARQLResources {

	private GeoSPARQLResources() {

	}

	public static final Resource RCC8_DC = ResourceFactory.createProperty(
			NS_GEO, rcc8dc);

	public static final Resource RCC8_EC = ResourceFactory.createProperty(
			NS_GEO, rcc8ec);

	public static final Resource RCC8_EQ = ResourceFactory.createProperty(
			NS_GEO, rcc8eq);

	public static final Resource RCC8_NTPP = ResourceFactory.createProperty(
			NS_GEO, rcc8ntpp);

	public static final Resource RCC8_NTPPi = ResourceFactory.createProperty(
			NS_GEO, rcc8ntppi);

	public static final Resource RCC8_PO = ResourceFactory.createProperty(
			NS_GEO, rcc8po);

	public static final Resource RCC8_TPP = ResourceFactory.createProperty(
			NS_GEO, rcc8tpp);

	public static final Resource RCC8_TPPi = ResourceFactory.createProperty(
			NS_GEO, rcc8tppi);

	static Resource[] RCC8_RESOURCES = { RCC8_DC, RCC8_EC, RCC8_EQ, RCC8_PO,
			RCC8_TPP, RCC8_TPPi, RCC8_NTPP, RCC8_NTPPi };

}
