package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.core.BasicPattern;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.main.StageGenerator;

/**
 * <p>
 * This class creates a {@link QueryIterator} for an arbitrary basic graph
 * pattern (BGP).
 * </p>
 * <p>
 * A bgp may contain triple patterns that match both classic and spatial
 * properties. The {@link QsrQueryIterFactory} creates the respective
 * {@link QueryIterator} for the specified {@link SpatialReasonerFactory}. The
 * {@link SpatialReasoner#handles(String)} method determines whether a spatial
 * reasoner handles the respective property or not.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public class QsrQueryIterFactory {

	private static final Logger _log = LoggerFactory
			.getLogger(QsrQueryIterFactory.class);

	public QueryIterator createQueryIterator(
			SpatialReasonerFactory reasonerFactory,
			StageGenerator defaultStageGenerator, BasicPattern bgp,
			QueryIterator input, ExecutionContext context) {
		final BasicPattern classicPattern = new BasicPattern();
		final BasicPattern classicPatternVariablePredicate = new BasicPattern();
		final BasicPattern spatialPattern = new BasicPattern();
		final SpatialReasoner reasoner = reasonerFactory.create(context);

		// Store join partners for spatial reasoning here.
		final Set<Var> classicPatternVariables = new HashSet<Var>();
		final Set<Var> spatialPatternVariables = new HashSet<Var>();

		// Compute results that are valid without reasoning.
		// These will be valid anyhow.
		final QueryIterQueued result = new QueryIterQueued();

		// Check all triple patterns whether we have to execute the reasoner.
		for (Triple tp : bgp.getList()) {
			final Node s = tp.getSubject();
			final Node p = tp.getPredicate();
			final Node o = tp.getObject();
			final String pUri = p.getURI();

			// For triple patterns with variable predicate we have to check
			// whether the predicate is to be joined with any classic triple
			// pattern.
			if (p.isVariable()) {
				classicPatternVariablePredicate.add(tp);
				spatialPattern.add(tp);
				if (s.isVariable()) {
					spatialPatternVariables.add((Var) s);
				}
				if (p.isVariable()) {
					spatialPatternVariables.add((Var) p);
				}
				if (o.isVariable()) {
					spatialPatternVariables.add((Var) o);
				}
			}
			// If this triple pattern requires spatial reasoning,
			// then add add it to the list of triple patters for which
			// we perform spatial reasoning.
			else if (reasoner.handles(pUri)) {
				spatialPattern.add(tp);
				if (s.isVariable()) {
					spatialPatternVariables.add((Var) s);
				}
				if (p.isVariable()) {
					spatialPatternVariables.add((Var) p);
				}
				if (o.isVariable()) {
					spatialPatternVariables.add((Var) o);
				}
			}
			// If triple pattern requires no spatial reasoning,
			// then add it to the classic part.
			else {
				classicPattern.add(tp);
				// Add all variable of the classical triple pattern,
				// since they may join with the spatial patterns.
				if (s.isVariable()) {
					classicPatternVariables.add((Var) s);
				}
				if (p.isVariable()) {
					classicPatternVariables.add((Var) p);
				}
				if (o.isVariable()) {
					classicPatternVariables.add((Var) o);
				}
			}
		}
		// Determine whether classic and spatial parts join.
		final Set<Var> patternVariablesIntersection = new HashSet<Var>(
				classicPatternVariables);
		patternVariablesIntersection.retainAll(spatialPatternVariables);
		final boolean spatialJoinsClassic = !patternVariablesIntersection
				.isEmpty();

		// If the bgp contains no spatial pattern, then there is nothing to do,
		// as we already added all classic results to the concatenated query
		// iterator.
		if (spatialPattern.isEmpty()) {
			_log.warn("Basic graph pattern {} is declared to be extended "
					+ "by a spatial reaonser "
					+ "but contains no according properties!", bgp);
		}
		// Add the joined result of spatial and classic patterns.
		else {
			// In case there exists a classic sub-pattern, we can perform the
			// classic pattern first.
			if (!classicPattern.isEmpty()) {
				final QueryIterator spatialInput = defaultStageGenerator
						.execute(classicPattern, input, context);

				// We can omit spatial reasoning, if the classic part has no
				// result but joins with the spatial part.
				if (!spatialJoinsClassic || spatialInput.hasNext()) {
					final QueryIterator resultWithReasoner = reasoner.execute(
							spatialPattern, spatialInput, context);
					result.add(resultWithReasoner);
				}
			}
			// In case there exist no classic parts of the pattern, we simply
			// execute the spatial reasoner.
			else {
				final QueryIterator resultWithReasoner = reasoner.execute(
						spatialPattern, input, context);
				result.add(resultWithReasoner);
			}
		}
		result.add(defaultStageGenerator.execute(bgp, input, context));
		return result;
	}
}
