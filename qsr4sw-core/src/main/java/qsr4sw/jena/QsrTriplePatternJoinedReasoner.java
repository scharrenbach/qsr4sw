package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterConcat;

public abstract class QsrTriplePatternJoinedReasoner extends
		QsrTriplePatternReasoner {

	protected abstract QueryIterator executeBoundTriplePattern(
			Triple spatialTp, ExecutionContext context);

	@Override
	protected QueryIterator executeTriplePattern(Triple spatialTp,
			QueryIterator input, ExecutionContext context) {
		// Only in case there exist classic results, we have to bind the
		// variables with teh results.
		if (input.hasNext()) {
			final QueryIterConcat result = new QueryIterConcat(context);
			Node s = spatialTp.getSubject();
			Node p = spatialTp.getPredicate();
			Node o = spatialTp.getObject();

			// Execute for all classic results.
			while (input.hasNext()) {
				Binding classicPatternResult = input.next();
				if (s.isVariable()) {
					if (classicPatternResult.contains((Var) s)) {
						s = classicPatternResult.get((Var) s);
					}
				}
				if (p.isVariable()) {
					if (classicPatternResult.contains((Var) p)) {
						p = classicPatternResult.get((Var) p);
					}
				}
				if (o.isVariable()) {
					if (classicPatternResult.contains((Var) o)) {
						o = classicPatternResult.get((Var) o);
					}
				}
				// Add new results by executing with bound variables.
				result.add(this.executeBoundTriplePattern(new Triple(s, p, o),
						context));
			}
			return result;
		}
		// If no classic results to join with, then execute spatial reasoner
		// only.
		else {
			return this.executeBoundTriplePattern(spatialTp, context);
		}
	}

}
