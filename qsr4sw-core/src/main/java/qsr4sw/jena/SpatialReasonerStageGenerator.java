package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.hp.hpl.jena.sparql.core.BasicPattern;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.main.StageGenerator;

public class SpatialReasonerStageGenerator implements StageGenerator {

	private final StageGenerator _defaultStageGenerator;

	private final SpatialReasonerFactory _spationalReasonerFactory;

	public SpatialReasonerStageGenerator(
			SpatialReasonerFactory spationalReasonerFactory,
			StageGenerator defaultStageGenerator) {
		_spationalReasonerFactory = spationalReasonerFactory;
		_defaultStageGenerator = defaultStageGenerator;
	}

	@Override
	public QueryIterator execute(BasicPattern bgp, QueryIterator input,
			ExecutionContext context) {
		QueryIterator result = new QsrQueryIterFactory().createQueryIterator(
				_spationalReasonerFactory, _defaultStageGenerator, bgp, input,
				context);
		return result;
	}

}
