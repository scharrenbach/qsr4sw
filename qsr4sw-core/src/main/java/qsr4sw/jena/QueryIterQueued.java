package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.jena.atlas.io.IndentedWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.serializer.SerializationContext;

/**
 * <p>
 * Query iterator that works on a collection of query iterators.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.1
 * @version 0.0.1
 * 
 */
public class QueryIterQueued implements QueryIterator {

	private static final Logger _log = LoggerFactory
			.getLogger(QueryIterQueued.class);

	private final Queue<QueryIterator> _queue;

	private QueryIterator _current;

	private boolean _closed;

	public QueryIterQueued() {
		this(null);
	}

	public QueryIterQueued(QueryIterator iter) {
		_queue = new LinkedBlockingQueue<QueryIterator>();
		_current = iter;
		_closed = false;
	}

	public QueryIterQueued add(QueryIterator iter) {
		if (_closed) {
			throw new RuntimeException();
		}
		if (_current == null) {
			_log.debug("Setting new initial iterator.");
			_current = iter;
		} else {
			_log.debug("Adding new iterator to queue.");
			_queue.add(iter);
		}
		return this;
	}

	@Override
	public void close() {
		if (_closed) {
			throw new RuntimeException();
		}
		_closed = true;
		_current = null;
		_queue.clear();
	}

	@Override
	public boolean hasNext() {
		while (_current != null && !_current.hasNext()) {
			_log.info("Trying to poll new iterator from queue...");
			if (!_queue.isEmpty()) {
				_log.debug("Polling new iterator from queue.");
				_current = _queue.poll();
			} else {
				_log.debug("Queue is empty, closing iterator.");
				_current = null;
			}
		}
		return _current != null && _current.hasNext();
	}

	@Override
	public Binding next() {
		Binding n = _current.next();
		_log.debug("Binding: {}", n);
		return n;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void output(IndentedWriter out, SerializationContext sCxt) {
		_current.output(out, sCxt);
	}

	@Override
	public String toString(PrefixMapping pmap) {
		return _current.toString(pmap);
	}

	@Override
	public void output(IndentedWriter out) {
		_current.output(out);
	}

	@Override
	public Binding nextBinding() {
		Binding n = _current.nextBinding();
		_log.debug("Binding: {}", n);
		return n;
	}

	@Override
	public void cancel() {
		_current.cancel();
	}

}
