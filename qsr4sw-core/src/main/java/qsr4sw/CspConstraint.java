package qsr4sw;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Basic Java bean for storing information about constraints.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.2
 * @version 0.0.2
 * 
 */
public class CspConstraint {

	private List<String> _ops;

	private Integer _leftOp;

	private Integer _rightOp;

	public CspConstraint(Integer leftOp, Integer rightOp, List<String> ops) {
		_leftOp = leftOp;
		_rightOp = rightOp;
		_ops = ops;
	}

	public CspConstraint() {
		this(-1, -1, new ArrayList<String>());
	}

	public void setLeftOp(Integer leftOp) {
		_leftOp = leftOp;
	}

	public Integer getLeftOp() {
		return _leftOp;
	}

	public void setRightOp(Integer rightOp) {
		_rightOp = rightOp;
	}

	public Integer getRightOp() {
		return _rightOp;
	}

	public void setOps(List<String> ops) {
		_ops = ops;
	}

	public List<String> getOps() {
		return _ops;
	}

	@Override
	public String toString() {
		StringBuffer opsBuffer = new StringBuffer();
		String space = "";
		for (String op : _ops) {
			opsBuffer.append(space + op);
			space = " ";
		}
		return String.format("%s %s ( %s )", _leftOp, _rightOp,
				opsBuffer.toString());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CspConstraint)) {
			return false;
		}
		final CspConstraint other = (CspConstraint) obj;
		return this._leftOp.equals(other._leftOp)
				&& this._rightOp.equals(other._rightOp)
				&& this._ops.equals(other._ops);
	}

}
