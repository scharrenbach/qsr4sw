package qsr4sw;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * <p>
 * Vocabulary for serializing constraint satisfaction problems.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.2
 * @since 0.0.2
 * 
 */
public class Csp {

	public static final String DC = "DC";
	public static final String EC = "EC";
	public static final String EQ = "EQ";
	public static final String PO = "PO";
	public static final String TPP = "TPP";
	public static final String NTPP = "NTPP";
	public static final String TPPI = "TPPI";
	public static final String NTPPI = "NTPPI";

}
