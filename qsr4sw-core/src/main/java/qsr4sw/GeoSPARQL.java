package qsr4sw;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * <p>
 * The complete GeoSPARQL vocabulary.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.1
 * @version 0.0.1
 * 
 */
public class GeoSPARQL {

	public static final String NS_GEO = "http://www.opengis.net/geosparql#";

	public static final String NS_GEOR = "http://www.opengis.net/def/rule/geosparql/";

	public static final String NS_GEOF = "http://www.opengis.net/def/function/geosparql/";

	/**
	 * Simple Features Relation Family (Sec 11.2)
	 */
	public static final String sfEquals = "sfEquals";
	/**
	 * Simple Features Relation Family (Sec 11.2)
	 */
	public static final String sfDisjoint = "sfDisjoint";
	/**
	 * Simple Features Relation Family (Sec 11.2)
	 */
	public static final String sfIntersects = "sfIntersects";
	/**
	 * Simple Features Relation Family (Sec 11.2)
	 */
	public static final String sfTouches = "sfTouches";
	/**
	 * Simple Features Relation Family (Sec 11.2)
	 */
	public static final String sfCrosses = "sfCrosses";
	/**
	 * Simple Features Relation Family (Sec 11.2)
	 */
	public static final String sfWithin = "sfWithin";
	/**
	 * Simple Features Relation Family (Sec 11.2)
	 */
	public static final String sfContains = "sfContains";
	/**
	 * Simple Features Relation Family (Sec 11.2)
	 */
	public static final String sfOverlaps = "sfOverlaps";

	/**
	 * Egenhofer Relation Family (Sec 11.3)
	 */
	public static final String ehEquals = "ehEquals";

	/**
	 * Egenhofer Relation Family (Sec 11.3)
	 */
	public static final String ehDisjoint = "ehDisjoint";

	/**
	 * Egenhofer Relation Family (Sec 11.3)
	 */
	public static final String ehMeet = "ehMeet";

	/**
	 * Egenhofer Relation Family (Sec 11.3)
	 */
	public static final String ehOverlap = "ehOverlap";

	/**
	 * Egenhofer Relation Family (Sec 11.3)
	 */
	public static final String ehCovers = "ehCovers";

	/**
	 * Egenhofer Relation Family (Sec 11.3)
	 */
	public static final String ehCoveredBy = "ehCoveredBy";

	/**
	 * Egenhofer Relation Family (Sec 11.3)
	 */
	public static final String ehInside = "ehInside";

	/**
	 * Egenhofer Relation Family (Sec 11.3)
	 */
	public static final String ehContains = "ehContains";

	/**
	 * RCC8 Relation Family (Sec 11.4)
	 */
	public static final String rcc8eq = "rcc8eq";

	/**
	 * RCC8 Relation Family (Sec 11.4)
	 */
	public static final String rcc8dc = "rcc8dc";

	/**
	 * RCC8 Relation Family (Sec 11.4)
	 */
	public static final String rcc8ec = "rcc8ec";

	/**
	 * RCC8 Relation Family (Sec 11.4)
	 */
	public static final String rcc8po = "rcc8po";

	/**
	 * RCC8 Relation Family (Sec 11.4)
	 */
	public static final String rcc8tppi = "rcc8tppi";

	/**
	 * RCC8 Relation Family (Sec 11.4)
	 */
	public static final String rcc8tpp = "rcc8tpp";

	/**
	 * RCC8 Relation Family (Sec 11.4)
	 */
	public static final String rcc8ntpp = "rcc8ntpp";

	/**
	 * RCC8 Relation Family (Sec 11.4)
	 */
	public static final String rcc8ntppi = "rcc8ntppi";

}
