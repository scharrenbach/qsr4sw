package qsr4sw;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
 * <p>
 * Implementation of the abstract test case for RCC( as defined by the <a
 * href="http://www.opengeospatial.org/standards/geosparql">GepSPARQL
 * specification</a>.
 * </p>
 * <p>
 * A.4.4 relation_family = RCC8 A.4.4.1 <br />
 * /conf/geometry-topology-extension/rcc8-query-functions <br />
 * Requirement: /req/geometry-topology-extension/rcc8-query-functions
 * Implementations shall support geof:rcc8eq, geof:rcc8dc, geof:rcc8ec, <br />
 * geof:rcc8po, geof:rcc8tppi, geof:rcc8tpp, geof:rcc8ntpp, geof:rcc8ntppi as
 * SPARQL extension functions, consistent with their corresponding DE-9IM
 * intersection patterns, as defined by Simple Features [ISO 19125-1]. <br />
 * a.) Test purpose: check conformance with this requirement <br />
 * b.) Test method: Verify that a set of SPARQL queries involving each of the
 * following functions returns the correct result for a test dataset when using
 * the specified serialization and version: geof:rcc8eq, geof:rcc8dc,
 * geof:rcc8ec, geof:rcc8po, geof:rcc8tppi, geof:rcc8tpp, geof:rcc8ntpp,
 * geof:rcc8ntppi. <br />
 * c.) Reference: Clause 9.5 Req 24 d.) Test Type: Capabilities <br />
 * </p>
 * 
 * @version 0.0.1
 * @since 0.0.1
 * @author Thomas Scharrenbach
 * 
 */
public interface GeoSPARQLTestCase {
	
	
}
