package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import static qsr4sw.jena.GeoSPARQLResources.RCC8_RESOURCES;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import qsr4sw.Csp;
import qsr4sw.CspConstraint;
import qsr4sw.CspNetwork;

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * <p>
 * Converts an rdf graph to a csp file.
 * </p>
 * <p>
 * CSP stands for constraint satisfaction problem.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.2
 * @since 0.0.2
 * 
 */
public class Rdf2Csp {

	private static final Map<Node, String> geosparql2csp = new HashMap<Node, String>();
	static {
		geosparql2csp.put(GeoSPARQLResources.RCC8_DC.asNode(), Csp.DC);
		geosparql2csp.put(GeoSPARQLResources.RCC8_EC.asNode(), Csp.EC);
		geosparql2csp.put(GeoSPARQLResources.RCC8_EQ.asNode(), Csp.EQ);
		geosparql2csp.put(GeoSPARQLResources.RCC8_PO.asNode(), Csp.PO);
		geosparql2csp.put(GeoSPARQLResources.RCC8_TPP.asNode(), Csp.TPP);
		geosparql2csp.put(GeoSPARQLResources.RCC8_NTPP.asNode(), Csp.NTPP);
		geosparql2csp.put(GeoSPARQLResources.RCC8_TPPi.asNode(), Csp.TPPI);
		geosparql2csp.put(GeoSPARQLResources.RCC8_NTPPi.asNode(), Csp.NTPPI);
	}

	public Rdf2Csp() {
	}

	/**
	 * Extract all triples with a spatial property and add each triple as a
	 * constraint.
	 * 
	 * @param graph
	 * @param out
	 * @throws IOException
	 */
	public CspNetwork convert(Graph graph) throws IOException {
		final CspNetwork net = new CspNetwork();
		final Map<Node, Integer> nodesHash = new HashMap<Node, Integer>();
		int nodesId = 0;
		final Map<SubjectObjectPair, Set<String>> subjectObjectHash = new HashMap<SubjectObjectPair, Set<String>>();
		for (Resource rcc8Res : RCC8_RESOURCES) {
			final ExtendedIterator<Triple> it = graph.find(null,
					rcc8Res.asNode(), null);
			while (it.hasNext()) {
				final Triple t = it.next();
				final Node subjectNode = t.getSubject();
				final Node predicateNode = t.getPredicate();
				final Node objectNode = t.getObject();
				// Check whether the subject and the object have already been
				// assigned a numeric id. If not, then do so.
				if (!nodesHash.keySet().contains(subjectNode)) {
					nodesHash.put(subjectNode, nodesId);
					++nodesId;
				}
				if (!nodesHash.keySet().contains(objectNode)) {
					nodesHash.put(objectNode, nodesId);
					++nodesId;
				}
				final SubjectObjectPair sop = new SubjectObjectPair(
						subjectNode, objectNode);
				if (!subjectObjectHash.containsKey(sop)) {
					subjectObjectHash.put(sop, new HashSet<String>());
				}
				// Add the predicate as a new constraint for the subject and the
				// object of the triple.
				final Set<String> constraints = subjectObjectHash.get(sop);
				constraints.add(geosparql2csp.get(predicateNode));
			}
		}

		for (Entry<SubjectObjectPair, Set<String>> entry : subjectObjectHash
				.entrySet()) {
			SubjectObjectPair sop = entry.getKey();
			int subjectId = nodesHash.get(sop.subjectNode);
			int objectId = nodesHash.get(sop.objectNode);
			CspConstraint c = new CspConstraint(subjectId, objectId,
					new ArrayList<String>(entry.getValue()));
			net.add(c);
		}
		return net;
	}

	private class SubjectObjectPair {
		public final Node subjectNode;
		public final Node objectNode;

		public SubjectObjectPair(Node subjectNode, Node objectNode) {
			this.subjectNode = subjectNode;
			this.objectNode = objectNode;
		}

		/**
		 * <p>
		 * Taken from <a href=
		 * "http://stackoverflow.com/questions/156275/what-is-the-equivalent-of-the-c-pairl-r-in-java"
		 * >stackoverflow.com</a>
		 * </p>
		 */
		public int hashCode() {
			int hashsubjectNode = subjectNode != null ? subjectNode.hashCode()
					: 0;
			int hashobjectNode = objectNode != null ? objectNode.hashCode() : 0;

			return (hashsubjectNode + hashobjectNode) * hashobjectNode
					+ hashsubjectNode;
		}

		/**
		 * <p>
		 * Taken from <a href=
		 * "http://stackoverflow.com/questions/156275/what-is-the-equivalent-of-the-c-pairl-r-in-java"
		 * >stackoverflow.com</a>
		 * </p>
		 */
		public boolean equals(Object other) {
			if (other instanceof SubjectObjectPair) {
				SubjectObjectPair otherPair = (SubjectObjectPair) other;
				return ((this.subjectNode == otherPair.subjectNode || (this.subjectNode != null
						&& otherPair.subjectNode != null && this.subjectNode
							.equals(otherPair.subjectNode))) && (this.objectNode == otherPair.objectNode || (this.objectNode != null
						&& otherPair.objectNode != null && this.objectNode
							.equals(otherPair.objectNode))));
			}

			return false;
		}

	}

}
