package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import qsr4sw.GeoSPARQL;

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.sparql.core.BasicPattern;
import com.hp.hpl.jena.sparql.core.DatasetGraph;
import com.hp.hpl.jena.sparql.core.DatasetGraphFactory;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.main.OpExecutorFactory;
import com.hp.hpl.jena.sparql.engine.main.StageGenerator;

/**
 * <p>
 * The dummy reasoner nswers queries on a pre-defined graph.
 * </p>
 * 
 * @version 0.0.1
 * @since 0.0.1
 * @author Thomas Scharrenbach
 * 
 */
public class DummyReasoner implements SpatialReasoner {

	private StageGenerator _defaultStageGenerator;

	private Graph _graph;

	public DummyReasoner(Graph graph, StageGenerator defaultStageGenerator) {
		_graph = graph;
		_defaultStageGenerator = defaultStageGenerator;
	}

	@Override
	public QueryIterator execute(BasicPattern spatialPattern,
			QueryIterator input, ExecutionContext context) {
		OpExecutorFactory factory = context.getExecutor();
		DatasetGraph dataset = DatasetGraphFactory.createOneGraph(_graph);
		//dataset.setDefaultGraph(_graph);
		ExecutionContext newContext = new ExecutionContext(
				context.getContext(), _graph, dataset, factory);
		QueryIterator result = _defaultStageGenerator.execute(spatialPattern,
				input, newContext);
//		System.out.println(result.hasNext());
//		while(result.hasNext()) {
//			System.out.println(result.next());
//		}
		return result;
	}

	@Override
	public boolean handles(String pUri) {
		String[] uris = { GeoSPARQL.NS_GEO + GeoSPARQL.rcc8dc,
				GeoSPARQL.NS_GEO + GeoSPARQL.rcc8ec,
				GeoSPARQL.NS_GEO + GeoSPARQL.rcc8eq,
				GeoSPARQL.NS_GEO + GeoSPARQL.rcc8po,
				GeoSPARQL.NS_GEO + GeoSPARQL.rcc8tpp,
				GeoSPARQL.NS_GEO + GeoSPARQL.rcc8ntpp,
				GeoSPARQL.NS_GEO + GeoSPARQL.rcc8tppi,
				GeoSPARQL.NS_GEO + GeoSPARQL.rcc8ntppi };
		Set<String> rcc8UriSet = new HashSet<String>(Arrays.asList(uris));

		return (rcc8UriSet.contains(pUri));
	}

}
