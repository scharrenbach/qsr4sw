package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import qsr4sw.CspConstraint;
import qsr4sw.CspNetwork;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

/**
 * <p>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.2
 * @version 0.0.2
 * 
 */
public class TestRdf2Csp {

	private static final Logger _log = LoggerFactory
			.getLogger(TestRdf2Csp.class);

	@Test(dataProvider = "dataQuerySolutionRecursive", dataProviderClass = DataQuerySolutionDataProvider.class)
	@Parameters({ "data", "query", "solution" })
	public void test(String data, String query, String solution) {
		try {
			// String filename =
			// "src/test/resources/rcc8/rcc8dc/testcase_ec_data.ttl";
			final Model dataModel = ModelFactory
					.createMemModelMaker()
					.createFreshModel()
					.read(new FileInputStream(data), "http://example.com",
							"TURTLE");
			final Model solutionModel = ModelFactory
					.createMemModelMaker()
					.createFreshModel()
					.read(new FileInputStream(solution), "http://example.com",
							"TURTLE");

			final Rdf2Csp rdf2Csp = new Rdf2Csp();

			String dataCsp = data.replace(".ttl", ".csp");
			OutputStream dataCspOut = new FileOutputStream(dataCsp);
			CspNetwork netData = rdf2Csp.convert(dataModel.getGraph());
			String nl = "";
			for (CspConstraint c : netData.getConstraints()) {
				dataCspOut.write(nl.getBytes());
				dataCspOut.write(c.toString().getBytes());
				nl = "\n";
			}
			dataCspOut.flush();
			dataCspOut.close();

			String solutionCsp = solution.replace(".ttl", ".csp");
			OutputStream solutionCspOut = new FileOutputStream(solutionCsp);
			CspNetwork netSolution = rdf2Csp.convert(solutionModel.getGraph());
			nl = "";
			for (CspConstraint c : netSolution.getConstraints()) {
				solutionCspOut.write(nl.getBytes());
				solutionCspOut.write(c.toString().getBytes());
				nl = "\n";
			}
			 solutionCspOut.flush();
			 solutionCspOut.close();

		} catch (Exception e) {
			_log.error("Error", e);
			assert false;
		}
	}

}
