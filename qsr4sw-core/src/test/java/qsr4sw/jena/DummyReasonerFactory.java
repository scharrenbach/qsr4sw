package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.main.StageGenerator;

/**
 * <p>
 * Factory for creating {@link DummyReasoner} objects.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.1
 * @since 0.0.1
 * 
 */
public class DummyReasonerFactory implements SpatialReasonerFactory {

	private StageGenerator _stageGenerator;
	
	private Graph _graph;

	public DummyReasonerFactory(Graph graph, StageGenerator stageGenerator) {
		_graph = graph;
		_stageGenerator = stageGenerator;
	}

	@Override
	public SpatialReasoner create(ExecutionContext context) {
		return new DummyReasoner(_graph, _stageGenerator);
	}
}
