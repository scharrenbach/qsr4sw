package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sparql.engine.main.StageGenerator;

/**
 * <p>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
public class JenaGeoSPARQLTestCase {

	private static final Logger _log = LoggerFactory
			.getLogger(JenaGeoSPARQLTestCase.class);

	@Test(dataProvider = "dataQuerySolutionRecursive", dataProviderClass = DataQuerySolutionDataProvider.class)
	@Parameters({ "data", "query", "solution" })
	public void test(String data, String query, String solution) {
		// String data = "src/test/resources/rcc8/rcc8eq/testcase01_data.ttl";
		// String solution =
		// "src/test/resources/rcc8/rcc8eq/testcase01_solution.ttl";
		// String query =
		// "src/test/resources/rcc8/rcc8eq/testcase01_query.sparql";
		_log.info(String
				.format("Starting test with data: '%s', query: '%s', and solution '%s'",
						data, query, solution));
		try {
			final File dataFile = new File(data);
			assert dataFile.isFile();
			assert dataFile.canRead();
			final File solutionFile = new File(solution);
			assert solutionFile.isFile();
			assert solutionFile.canRead();
			final File queryFile = new File(query);
			assert solutionFile.isFile();
			assert solutionFile.canRead();

			_log.info("Loading data file {}", dataFile.getPath());
			final Model dataModel = ModelFactory.createDefaultModel().read(
					dataFile.toURI().toString(), "TURTLE");

			_log.info("Loading solution file {}", solutionFile.getPath());
			final Model solutionModel = ModelFactory.createDefaultModel().read(
					solutionFile.toURI().toString(), "TURTLE");

			_log.info("Loading query file {}", queryFile.getPath());
			String line = null;
			final StringBuffer readBuffer = new StringBuffer();
			final BufferedReader reader = new BufferedReader(new FileReader(
					queryFile));
			while ((line = reader.readLine()) != null) {
				readBuffer.append(line);
			}
			reader.close();
			final String queryString = readBuffer.toString();

			_log.info("Creating spatial reasoner factory");
			// Get ARQ's standard stage generator.
			final StageGenerator standardStageGenerator = (StageGenerator) ARQ
					.getContext().get(ARQ.stageGenerator);
			final SpatialReasonerFactory reasonerFactory = new DummyReasonerFactory(
					solutionModel.getGraph(), standardStageGenerator);

			// Create the stage generator that is capable of handling spatial
			// reasoning.
			final StageGenerator spatialStageGenerator = new SpatialReasonerStageGenerator(
					reasonerFactory, standardStageGenerator);
			assert spatialStageGenerator != null;

			_log.info(
					"Creating query execution with spatial reasoning for query: {}",
					queryString);
			// We have to use the data model here, since the spatial reasoning
			// shall insert the missing results.
			final QueryExecution qexecSpatialReasoning = QueryExecutionFactory
					.create(queryString, dataModel);

			// Register the stage generator with ARQ on a per-query basis.
			// StageBuilder.setGenerator(qexecSpatialReasoning.getContext(),
			// spatialStageGenerator);

			qexecSpatialReasoning.getContext().set(ARQ.stageGenerator,
					spatialStageGenerator);
			qexecSpatialReasoning.getContext().set(ARQ.optimization, false);

			_log.info("Executing query: {}", queryString);
			final Model resultModelSpatialReasoning = qexecSpatialReasoning
					.execConstruct();

			_log.info("Iterating over result for query: {}", queryString);
			resultModelSpatialReasoning.write(System.out, "TURTLE");

			// solutionModel.difference(resultModelSpatialReasoning).write(System.out,
			// "TURTLE");
			// System.exit(1);
			// assert
			// solutionModel.isIsomorphicWith(resultModelSpatialReasoning);

		} catch (Exception e) {
			e.printStackTrace();
			_log.error("Error", e);
			assert false;
		}

	}
}
