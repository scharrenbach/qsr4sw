package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-core
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;

/**
 * <p>
 * TestNG {@link DataProvider} for test cases with data, query and solution
 * files. See also the <a href=
 * "http://testng.org/doc/documentation-main.html#parameters-dataproviders"
 * >testng documentation</a>.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.1
 * @version 0.0.1
 * 
 */
public class DataQuerySolutionDataProvider {

	private static final Logger _log = LoggerFactory
			.getLogger(DataQuerySolutionDataProvider.class);
	protected static final String DATA_SUFFIX = "_data.ttl";
	protected static final String QUERY_SUFFIX = "_query.sparql";
	protected static final String SOLUTION_SUFFIX = "_solution.ttl";

	/**
	 * <p>
	 * Visit the specified directory recursively and check it and its
	 * subdirectories for valid data, query, and solution combinations.
	 * </p>
	 * 
	 * @return
	 */
	@DataProvider(name = "dataQuerySolutionRecursive")
	public static Object[][] provideDataForDirectories() {
		String dir = "src/test/resources/rcc8";

		try {
			final File dataRootDir = new File(dir);
			assert dataRootDir.exists();
			assert dataRootDir.isDirectory();
			assert dataRootDir.canRead();
			List<Object[]> result = visitDir(dataRootDir);
			return result.toArray(new Object[result.size()][]);
		} catch (Exception e) {
			_log.error("Error in data provider!", e);
			assert false;
		}
		return null;
	}

	/**
	 * <p>
	 * Recursively visit all subdirectories and then all potential data, query
	 * and solution files.
	 * </p>
	 * 
	 * @param dir
	 * @return
	 */
	public static List<Object[]> visitDir(File dir) {
		final List<Object[]> result = new LinkedList<Object[]>();
		final File[] subDirs = dir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory();
			}
		});
		for (File sub : subDirs) {
			result.addAll(visitDir(sub));
		}
		result.addAll(visitFiles(dir));
		return result;
	}

	/**
	 * <p>
	 * List all valid combinations of data, query, and solution files.
	 * </p>
	 * 
	 * @param dir
	 * @return
	 */
	public static List<Object[]> visitFiles(File dir) {
		_log.info("Scanning directory '{}'.", dir);
		final List<Object[]> result = new ArrayList<Object[]>();
		final File[] dataFiles = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File path, String name) {
				return name.endsWith(DATA_SUFFIX);
			}
		});

		for (File df : dataFiles) {
			_log.info("Found data file '{}'", df);
			final String dfName = df.getName();
			final String dfPrefix = dfName.substring(0,
					dfName.lastIndexOf(DATA_SUFFIX));
			final File[] queryFiles = dir.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File path, String name) {
					return name.startsWith(dfPrefix + "_")
							&& name.endsWith(QUERY_SUFFIX);
				}
			});
			final File[] solutionFiles = dir.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File path, String name) {
					return name.startsWith(dfPrefix + "_")
							&& name.endsWith(SOLUTION_SUFFIX);
				}
			});
			for (File qf : queryFiles) {
				for (File sf : solutionFiles) {
					_log.info("Found query file '{}'", qf);
					_log.info("Found solution file '{}'", sf);
					final Object[] res = { df.getAbsolutePath(),
							qf.getAbsolutePath(), sf.getAbsolutePath() };
					result.add(res);
				}
			}
		}
		return result;
	}

	//
	//
	//

	private DataQuerySolutionDataProvider() {

	}

}
