package qsr;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Scharrenbach.Net
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import qsr4sw.jena.function.NTPP;
import qsr4sw.jena.function.RCC8;
import qsr4sw.jena.function.RCC8Server;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sparql.function.FunctionRegistry;

public class TestQSRFunction {

	private static final Logger _log = LoggerFactory
			.getLogger(TestQSRFunction.class);

	// fast ugly hack
	private static String generateDB() {
		StringBuffer result = new StringBuffer();

		// String[] cities = { DB_NS + "A", DB_NS + "B", DB_NS + "C" };
		//
		// String[] parts = { DB_NS + "x", DB_NS + "y", DB_NS + "z" };
		//
		result.append("(" + RCC8.NS + "x" + " ntpp " + RCC8.NS + "A" + ")");
		result.append("(" + RCC8.NS + "y" + " ntpp " + RCC8.NS + "A" + ")");

		// for (int i = 0; i < cities.length; ++i) {
		// for (int j = 0; j < parts.length; ++j) {
		// if (new Random().nextBoolean()) {
		// result.append("(" + cities[i] + " ntpp ");
		// }
		// }
		//
		// }

		return result.toString();
	}

	@Test
	public void test() {
		try {
			String hostname = "localhost";
			int port = 5678;
			RCC8Server rcc8Server = new RCC8Server(hostname, port);
			InputStream rcc8Data = new BufferedInputStream(new FileInputStream(
					"src/test/resources/csps2_small.lisp"));
			
			rcc8Server.loadDataLisp(rcc8Data);
			
			FunctionRegistry registry = FunctionRegistry.get();
			Model model = ModelFactory.createDefaultModel();

			String ns = "http://qsr.org/functions#";
			String funcname = "partOf";
			String city = String.format("<%sA>", NTPP.NS);
			registry.put(ns + "partOf", NTPP.class);
			final String queryString = String.format(""
					+ "PREFIX my:<%s>                   \n"
					+ "SELECT ?part                     \n"
					+ "WHERE {                          \n"
					+ "BIND( <%s%s>( %s ) AS ?result ) .\n"
					+ "}                                  ", ns, ns, funcname,
					city);
			_log.info("Executing query:\n {}", queryString);
			final Query query = QueryFactory.create(queryString);
			final QueryExecution qexec = QueryExecutionFactory.create(query,
					model);
			try {
				ResultSet results = qexec.execSelect();
				for (; results.hasNext();) {
					QuerySolution soln = results.nextSolution();
					RDFNode computedResultData = soln.get("?part");
				}
			} finally {
				qexec.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
			_log.error("Error", e);
			assert false;
		}
	}

}
