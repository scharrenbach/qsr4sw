package qsr4sw.sparq;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import qsr4sw.CspConstraint;
import qsr4sw.CspNetwork;
import qsr4sw.exec.ProcessCspExecutor;
import qsr4sw.exec.ProcessCspExecutorInputStream;
import qsr4sw.jena.Csp2Rdf;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.rdf.model.ModelFactory;

/**
 * <p>
 * Test the process based connector to PyRCC8.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.2
 * @version 0.0.2
 * 
 */
public class TestSparqConnector {

	private static final Logger _log = LoggerFactory
			.getLogger(TestSparqConnector.class);

	@Test
	public void test() {
		try {
			String cspfile = "src/test/resources/sparq.in";
			final BufferedInputStream input = new BufferedInputStream(
					new FileInputStream(new File(cspfile)));

			String optPrint = "constraint-reasoning RCC8 algebraic-closure";

			String[] args = { optPrint };
			String executable = "/Users/scharrenbach/Documents/workspace/dagstuhl-qr/sparq/SparQ_0.7.5/sparq";

			final ProcessCspExecutor reasoner = new ProcessCspExecutorInputStream(
					executable, new SparqOutputParser.Factory());
			final List<CspConstraint> constraints = reasoner.execute(args,
					input);
			CspNetwork net = new CspNetwork(constraints);
			System.out.println(constraints);
			Map<String, Node> csp2RdfNodeMap = new HashMap<String, Node>();
			csp2RdfNodeMap.put("0",
					NodeFactory.createURI("http://example.com/0"));
			csp2RdfNodeMap.put("1",
					NodeFactory.createURI("http://example.com/1"));
			csp2RdfNodeMap.put("2",
					NodeFactory.createURI("http://example.com/2"));
			ModelFactory.createModelForGraph(new Csp2Rdf(csp2RdfNodeMap).convert(net)).write(
					System.out);

		} catch (Exception e) {
			_log.error("Error", e);
			assert false;
		}
	}

}
