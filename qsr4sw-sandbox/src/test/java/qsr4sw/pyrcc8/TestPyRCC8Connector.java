package qsr4sw.pyrcc8;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import qsr4sw.CspConstraint;
import qsr4sw.exec.ProcessCspExecutor;
import qsr4sw.exec.ProcessCspExecutorInputStream;

/**
 * <p>
 * Test the process based connector to PyRCC8.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.2
 * @version 0.0.2
 * 
 */
public class TestPyRCC8Connector {

	private static final Logger _log = LoggerFactory
			.getLogger(TestPyRCC8Connector.class);

	@Test
	public void test() {
		try {
			String cspfile = "src/test/resources/testcase_eq_solution.csp";
			final BufferedInputStream input = new BufferedInputStream(
					new FileInputStream(new File(cspfile)));

			String pyrcc8path = "/Users/scharrenbach/Documents/workspace/dagstuhl-qr/pyrcc8/"
					+ "PyRCC8/" + "rcc8.py";
			String optPrint = "-d";

			String[] args = { pyrcc8path, optPrint };
			String executable = "python";

			final ProcessCspExecutor reasoner = new ProcessCspExecutorInputStream(
					executable, new PyRCC8OutputParser.Factory());
			final List<CspConstraint> constraints = reasoner.execute(args,
					input);
			System.out.println(constraints);
		} catch (Exception e) {
			_log.error("Error", e);
			assert false;
		}
	}

}
