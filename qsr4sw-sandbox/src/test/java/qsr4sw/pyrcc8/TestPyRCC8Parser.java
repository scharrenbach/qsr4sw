package qsr4sw.pyrcc8;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.FileInputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import qsr4sw.CspConstraint;

/**
 * <p>
 * Test for {@link PyRCC8OutputParser}
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.2
 * @version 0.0.2
 * 
 */
public class TestPyRCC8Parser {

	private static final Logger _log = LoggerFactory
			.getLogger(TestPyRCC8Parser.class);

	@Test
	public void test() {
		String pyrcc8out = "src/test/resources/pyrcc8.out";

		try {
			final PyRCC8OutputParser parser = new PyRCC8OutputParser();

			final List<CspConstraint> constraintsList = parser
					.parse(new FileInputStream(pyrcc8out));

			for (CspConstraint constraint : constraintsList) {
				System.out.println(constraint);
			}

		} catch (Exception e) {
			_log.error("Error", e);
			assert false;
		}
	}

}
