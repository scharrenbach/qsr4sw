package qsr4sw.pyrcc8;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.FileInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import qsr4sw.CspNetwork;
import qsr4sw.CspNetworkParser;
import qsr4sw.CspNetworkSerializer;
import qsr4sw.StringCspNetworkSerializer;

/**
 * 
 * <p>
 * This class tests the {@link CspNetworkSerializer} interface.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 * @version 0.0.2
 * @since 0.0.2
 */
public class TestCspNetworkSerializer {

	private static final Logger _log = LoggerFactory
			.getLogger(TestCspNetworkSerializer.class);

	@Test
	public void test() {
		try {
			String sourcefile = "src/test/resources/serializer/source.csp";
			String targetFile = "src/test/resources/serializer/target.csp";
			CspNetworkParser parser = new CspNetworkParser();

			final CspNetwork net = parser
					.parse(new FileInputStream(sourcefile));
			final CspNetworkSerializer serializer = new StringCspNetworkSerializer();
			//ByteArrayOutputStream out = new ByteArrayOutputStream();
			serializer.serialize(net, System.out);
			//System.out.println(new String(out.toByteArray()));

		} catch (Exception e) {
			_log.error("", e);
		}
	}

}
