grammar AntlrPyRCC8Output;
 
// Parser Rules
 
typeId : TYPE_ID ;

visitedNodes : VISITED_NODES NUMBER;

visitedArcs : VISITED_ARCS NUMBER;

checkedConstraints : CHECKED_CONSTRAINTS NUMBER;

consistent : CONSISTENT ;

rcc8Op :  QUOTE RCC8_OP QUOTE ;

rcc8Line : NUMBER NUMBER LEFT_BRACE rcc8Op ( COMMA rcc8Op )* RIGHT_BRACE ;

pyRcc8Output :	SEPARATOR NEWLINE
				typeId 
				SEPARATOR NEWLINE
				visitedNodes NEWLINE
				visitedArcs NEWLINE
				checkedConstraints NEWLINE
				SEPARATOR NEWLINE
				consistent NEWLINE
				( rcc8Line NEWLINE )+ 
				SEPARATOR ( NEWLINE )*;
 
// Lexer Rules
 
TEXT : ('a'..'z')+ ;

NUMBER : ('0'..'9')+ ;

NEWLINE : ( '\r' | '\n' ) ;
 
COMMENT : '#' ~[\r\n]* '\r'? '\n' -> skip ;

WHITESPACE : ( '\t' | ' ' | '\u000C' )+ -> skip ;

LEFT_BRACE : '[' ;

RIGHT_BRACE : ']' ;

COMMA : ',' ;

QUOTE : '\'' ;

RCC8_OP : 'DC' | 'EC' | 'EQ' | 'PO' | 'TPPI' | 'NTPPI' | 'TPP' | 'NTPP' ;

HASHTAG : '#' ;

SEPARATOR : '--------------------------------------' ;

TYPE_ID : 'Type ID:' ;

VISITED_NODES : 'Visited nodes:' ;

VISITED_ARCS : 'Visited arcs:' ;

CHECKED_CONSTRAINTS : 'Checked constraints:' ;

CONSISTENT : 'Consistent';
