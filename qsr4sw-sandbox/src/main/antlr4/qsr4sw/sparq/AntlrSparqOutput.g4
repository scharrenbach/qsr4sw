grammar AntlrSparqOutput;
 
// Parser Rules
 
rcc8Op :  LEFT_BRACE ( RCC8_OP )+ RIGHT_BRACE ;

rcc8Line : LEFT_BRACE ( SYMBOL )+ rcc8Op ( SYMBOL )+ RIGHT_BRACE ;

sparqOutput :	( SYMBOL )*
				LEFT_BRACE
				( rcc8Line )*
				RIGHT_BRACE ;
 
// Lexer Rules

WHITESPACE : ( '\t' | ' ' | '\u000C' | '\n' | '\r' )+ -> skip ;

LEFT_BRACE : '(' ;

RIGHT_BRACE : ')' ;

RCC8_OP : 'dc' | 'ec' | 'eq' | 'po' | 'tppi' | 'tpp' | 'ntppi' | 'ntpp' ;
 
SYMBOL : ~')' | ~'(';

// COMMENT : ~[LEFT_BRACE]* -> skip ;


