grammar AntlrCsp;

cspProperty : RCC8_OP ;

cspConstraintLine : NUMBER NUMBER LEFT_BRACE cspProperty* RIGHT_BRACE ;

cspConstraints :	NUMBER 
					( cspConstraintLine NEWLINE )+ 
					( NEWLINE )* 
					FULLSTOP;
 


// Lexer Rules
 
TEXT : ('a'..'z')+ ;

NUMBER : ('0'..'9')+ ;

NEWLINE : ( '\r' | '\n' ) ;
 
HASHTAG : '#' ;

COMMENT : HASHTAG ~[\r\n]* '\r'? '\n' -> skip ;

WHITESPACE : ( '\t' | ' ' | '\u000C' )+ -> skip ;

LEFT_BRACE : '(' ;

RIGHT_BRACE : ')' ;

COMMA : ',' ;

QUOTE : '\'' ;

RCC8_OP : 'DC' | 'EC' | 'EQ' | 'PO' | 'TPPI' | 'NTPPI' | 'TPP' | 'NTPP' ;


SEPARATOR : '--------------------------------------' ;

TYPE_ID : 'Type ID:' ;

VISITED_NODES : 'Visited nodes:' ;

VISITED_ARCS : 'Visited arcs:' ;

CHECKED_CONSTRAINTS : 'Checked constraints:' ;

CONSISTENT : 'Consistent';

FULLSTOP : '.' ;

