package qsr4sw;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * 
 * <p>
 * This class serializes a {@link CspNetwork} to a String.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 * @version 0.0.2
 * @since 0.0.2
 */
public class StringCspNetworkSerializer implements CspNetworkSerializer {

	@Override
	public void serialize(CspNetwork net, OutputStream out) throws IOException {
		final OutputStream outWrite = (out instanceof BufferedOutputStream) ? out
				: new BufferedOutputStream(out);
		final List<CspConstraint> constraintsList = net.getConstraints();
		outWrite.write((net.getMaxNode() + " # " + "\n").getBytes());
		for (CspConstraint constraint : constraintsList) {
			outWrite.write((constraint.toString() + "\n").getBytes());
		}
		outWrite.write(".".getBytes());
		outWrite.flush();
		if (outWrite != System.out) {
			outWrite.close();
		}
	}

}
