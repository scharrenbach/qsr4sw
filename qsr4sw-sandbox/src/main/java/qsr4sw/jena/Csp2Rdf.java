package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

import qsr4sw.CspConstraint;
import qsr4sw.CspNetwork;

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.mem.GraphMem;
import com.hp.hpl.jena.rdf.model.AnonId;

/**
 * 
 * <p>
 * This class converts a csp network to an RDF graph.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 * @version 0.0.2
 * @since 0.0.2
 */
public class Csp2Rdf {

	private final Map<String, Node> _cspNode2RdfNodeMap;

	public Csp2Rdf() {
		_cspNode2RdfNodeMap = new HashMap<String, Node>();
	}

	public Csp2Rdf(Map<String, Node> cspNode2RdfNodeMap) {
		this();
		_cspNode2RdfNodeMap.putAll(cspNode2RdfNodeMap);
	}

	public Graph convert(CspNetwork net) {
		final Graph result = new GraphMem();
		for (CspConstraint con : net.getConstraints()) {
			final String leftOp = con.getLeftOp().toString();
			final String rightOp = con.getRightOp().toString();
			final Node leftNode = _cspNode2RdfNodeMap.containsKey(leftOp) ? _cspNode2RdfNodeMap
					.get(leftOp) : NodeFactory
					.createAnon(AnonId.create(leftOp));
			final Node rightNode = _cspNode2RdfNodeMap.containsKey(rightOp) ? _cspNode2RdfNodeMap
					.get(rightOp) : NodeFactory.createAnon(AnonId
					.create(rightOp));
			for (String op : con.getOps()) {
				Node opNode = CspGeoSPARQL.csp2geosparql.get(op.toUpperCase())
						.asNode();
				result.add(new Triple(leftNode, opNode, rightNode));
			}
		}
		return result;
	}

}
