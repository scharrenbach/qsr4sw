package qsr4sw.jena.function;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Scharrenbach.Net
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.sparql.expr.NodeValue;
import com.hp.hpl.jena.sparql.function.FunctionBase1;

public abstract class RCC8 extends FunctionBase1 {

	private static final Logger _log = LoggerFactory.getLogger(RCC8.class);

	//public static final String NS = "http://www.ontotext.com/proton/protontop#";
	public static final String NS = "http://www.opengis.net/ont/geosparql#";

	
	
	public static final String PO = NS + "partOf";
	public static final String DC = NS + "DC";
	public static final String EC = NS + "EC";
	public static final String EQ = NS + "EQ";
	public static final String NTPP = NS + "NTPP";
	public static final String TPP = NS + "TPP";
	public static final String NTPPi = NS + "NTPPi";
	public static final String TPPi = NS + "TPPi";

	protected abstract String getOp();

	@Override
	public NodeValue exec(NodeValue arg) {
		String op = getOp();

		String dummyQuery = String.format(
				"constraint-reasoning rcc8 match ((x %s %s)) %s", op, arg
						.asNode().getURI(), "$var");

		_log.info("Created query {}", dummyQuery);
		try {
			String hostname = "192.168.10.69";
			int port = 5678;
			_log.info("Open socket connection to {}:{}", hostname, port);
			Socket socket = new Socket(hostname, port);
			OutputStream out = socket.getOutputStream();
			out.write(dummyQuery.getBytes());
			InputStream in = socket.getInputStream();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			System.out.println();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
