package qsr4sw.jena.function;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Scharrenbach.Net
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

public class RCC8Server {

	private static Map<String, String> createRegistry() {
		Map<String, String> registry = new HashMap<String, String>();
		registry.put(RCC8.NS + DC.NAME, "dc");
		registry.put(RCC8.NS + EC.NAME, "ec");
		registry.put(RCC8.NS + EQ.NAME, "eq");
		registry.put(RCC8.NS + NTPP.NAME, "ntpp");
		registry.put(RCC8.NS + NTTPi.NAME, "ntppi");
		registry.put(RCC8.NS + PO.NAME, "po");
		registry.put(RCC8.NS + TPP.NAME, "tpp");
		registry.put(RCC8.NS + TPPi.NAME, "tppi");
		return registry;
	}

	public static final Map<String, String> registry = Collections
			.unmodifiableMap(createRegistry());

	private String _hostname;

	private int _port;

	private Socket _socket;

	public RCC8Server(String hostname, int port) {
		_hostname = hostname;
		_port = port;
		_socket = new Socket();
	}

	public void init() throws IOException {
		SocketAddress address = new InetSocketAddress(_hostname, _port);
		_socket.bind(address);
	}

	public void loadDataRDF(InputStream in, String uri)
			throws UnknownHostException, IOException {
		Socket socket = null;
		try {
			final Model model = ModelFactory.createDefaultModel();
			model.read(in, uri);
			socket = new Socket(_hostname, _port);
			final OutputStream out = new BufferedOutputStream(
					socket.getOutputStream());
			StmtIterator it = model.listStatements();
			StringBuffer buffer = new StringBuffer();
			buffer.append("let $var = ( ");
			while (it.hasNext()) {
				Statement stmt = it.next();

				buffer.append(String.format("( %s %s %s )", stmt.getSubject(),
						URI.create(stmt.getPredicate().getURI()).getFragment(),
						stmt.getObject()));
			}
			buffer.append(" )");
			out.write(buffer.toString().getBytes());
			out.flush();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			InputStream sockIn = socket.getInputStream();
			int bytesSockinRead = -1;
			while ((bytesSockinRead = sockIn.read()) > 0) {

			}

		} catch (UnknownHostException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} finally {
			// if (socket != null) {
			// socket.close();
			// }
		}
	}

	public void reconnect(int tries) {
		int timeout = 10;
		for (int i = 0; i < tries; ++i) {
			if (!_socket.isConnected()) {
				SocketAddress address = new InetSocketAddress(_hostname, _port);
				try {
					_socket.connect(address, timeout);
				} catch (SocketTimeoutException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void loadDataLisp(InputStream in) throws UnknownHostException,
			IOException {
		if (!_socket.isConnected()) {
			reconnect(3);
		}
		if (_socket.isConnected()) {
			try {
				final OutputStream out = new BufferedOutputStream(
						_socket.getOutputStream());
				// new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				out.write("let var = ".getBytes());
				int bytesRead = -1;
				while ((bytesRead = in.read(buffer)) > 0) {
					out.write(buffer, 0, bytesRead);
				}
				out.write("\n".getBytes());
				out.flush();
				// System.out.println(new String(out.toByteArray()));
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//				InputStream sockIn = _socket.getInputStream();
//				int bytesSockinRead = -1;
//				byte[] socketInBuffer = new byte[64];
//				while ((bytesSockinRead = sockIn.read(socketInBuffer)) > 0) {
//					System.out.print(new String(socketInBuffer));
//				}
			} catch (UnknownHostException e) {
				throw e;
			} catch (IOException e) {
				throw e;
			}
		}
	}
}
