package qsr4sw.jena.function;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Scharrenbach.Net
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class PO extends RCC8 {

	public static final String NAME = "rcc8po";

	@Override
	protected String getOp() {
		return NAME;
	}

}
