package qsr4sw.jena;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import qsr4sw.Csp;

import com.hp.hpl.jena.rdf.model.Resource;

public class CspGeoSPARQL {

	private CspGeoSPARQL() {
	}

	private static final Map<String, Resource> registryCsp2geosparql = new HashMap<String, Resource>();
	static {
		registryCsp2geosparql.put(Csp.DC, GeoSPARQLResources.RCC8_DC);
		registryCsp2geosparql.put(Csp.EC, GeoSPARQLResources.RCC8_EC);
		registryCsp2geosparql.put(Csp.EQ, GeoSPARQLResources.RCC8_EQ);
		registryCsp2geosparql.put(Csp.PO, GeoSPARQLResources.RCC8_PO);
		registryCsp2geosparql.put(Csp.TPP, GeoSPARQLResources.RCC8_TPP);
		registryCsp2geosparql.put(Csp.NTPP, GeoSPARQLResources.RCC8_NTPP);
		registryCsp2geosparql.put(Csp.TPPI, GeoSPARQLResources.RCC8_TPPi);
		registryCsp2geosparql.put(Csp.NTPPI, GeoSPARQLResources.RCC8_NTPPi);
	}
	public static final Map<String, Resource> csp2geosparql = Collections
			.unmodifiableMap(registryCsp2geosparql);

	private static final Map<Resource, String> registryGeosparql2csp = new HashMap<Resource, String>();
	static {
		registryGeosparql2csp.put(GeoSPARQLResources.RCC8_DC,
				Csp.DC);
		registryGeosparql2csp.put(GeoSPARQLResources.RCC8_EC,
				Csp.EC);
		registryGeosparql2csp.put(GeoSPARQLResources.RCC8_EQ,
				Csp.EQ);
		registryGeosparql2csp.put(GeoSPARQLResources.RCC8_PO,
				Csp.PO);
		registryGeosparql2csp.put(GeoSPARQLResources.RCC8_TPP,
				Csp.TPP);
		registryGeosparql2csp.put(GeoSPARQLResources.RCC8_NTPP,
				Csp.NTPP);
		registryGeosparql2csp.put(GeoSPARQLResources.RCC8_TPPi,
				Csp.TPPI);
		registryGeosparql2csp.put(GeoSPARQLResources.RCC8_NTPPi,
				Csp.NTPPI);
	}
	public static final Map<Resource, String> geosparql2csp = Collections
			.unmodifiableMap(registryGeosparql2csp);

}
