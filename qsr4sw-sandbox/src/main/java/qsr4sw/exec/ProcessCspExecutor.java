package qsr4sw.exec;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qsr4sw.CspConstraint;
import qsr4sw.pyrcc8.PyRCC8OutputParser;

/**
 * <p>
 * Process based executor implementation.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.2
 * @version 0.0.2
 * 
 */
public abstract class ProcessCspExecutor {

	private static final Logger _log = LoggerFactory
			.getLogger(ProcessCspExecutor.class);

	private final String _executable;
	
	private final CspExecutorOutputParser.Factory _parserFactory; 

	private long _timeout = 10000L;

	public ProcessCspExecutor(String executable, CspExecutorOutputParser.Factory parserFactory) {
		_executable = executable;
		_parserFactory = parserFactory;
	}

	public void setTimeout(long timeout) {
		_timeout = timeout;
	}

	public long getTimeout() {
		return _timeout;
	}

	public List<CspConstraint> execute(String[] args, InputStream in) {
		Process proc = null;
		try {
			String[] aaa = new String[args.length + 1];
			aaa[0] = _executable;
			for (int i = 0; i < args.length; ++i) {
				aaa[i + 1] = args[i];
			}

			// Create a new Processbuilder for the executable and the command
			// line arguments.
			final ProcessBuilder pb = new ProcessBuilder(aaa);

			// Create the parser that parses the output of the process.
			final CspExecutorOutputParser parser = _parserFactory.createParser();

			// Start executing the process.
			_log.info("Starting process for executable '{}'", _executable);
			proc = pb.start();

			// Capture the output of the process in a thread of its own.
			final BufferedInputStream processOutput = new BufferedInputStream(
					proc.getInputStream());
			final Callable<List<CspConstraint>> parserCall = new Callable<List<CspConstraint>>() {

				@Override
				public List<CspConstraint> call() throws Exception {
					try {
						final List<CspConstraint> constraints = parser
								.parse(processOutput);
						return constraints;
					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				}
			};
			final FutureTask<List<CspConstraint>> parserTask = new FutureTask<List<CspConstraint>>(
					parserCall);
			final Thread parserThread = new Thread(parserTask);
			parserThread.start();

			// Add data from STDIN to the process if required.
			performExecution(proc, in);

			// Capture the parsed output from the process.
			final List<CspConstraint> constraints = parserTask.get(
					getTimeout(), TimeUnit.MILLISECONDS);
			parserThread.join();
			return constraints;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		// This block can only be reached when an exception was thrown.
		// In that case we must destroy the process.
		finally {
			if (proc != null) {
				proc.destroy();
			}
		}
	}

	protected abstract void performExecution(Process proc, InputStream in);
}
