package qsr4sw.exec;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Simple connector to PyRCC8.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.2
 * @version 0.0.2
 * 
 */
public class ProcessCspExecutorInputStream extends ProcessCspExecutor {

	public ProcessCspExecutorInputStream(String executable, CspExecutorOutputParser.Factory parserFactory) {
		super(executable, parserFactory);
	}

	private static final Logger _log = LoggerFactory
			.getLogger(ProcessCspExecutorInputStream.class);

	@Override
	protected void performExecution(Process proc, InputStream in) {
		try {
			final BufferedReader input = new BufferedReader(
					new InputStreamReader(in));
			final BufferedOutputStream processInput = new BufferedOutputStream(
					proc.getOutputStream());

			final Runnable processRun = new Runnable() {
				@Override
				public void run() {
					String inputLine = null;
					try {
						while ((inputLine = input.readLine()) != null) {
							processInput.write(inputLine.getBytes());
							processInput.flush();
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						try {
							processInput.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							input.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					_log.info("Input file read.");
				}
			};
			final Thread processThread = new Thread(processRun);
			processThread.start();
			processThread.join();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
