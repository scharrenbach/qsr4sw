package qsr4sw;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import qsr4sw.AntlrCspParser.CspConstraintsContext;
import qsr4sw.pyrcc8.AntlrPyRCC8OutputLineListener;

/**
 * 
 * <p>This class parses csp files to {@link CspNetwork} objects.</p>
 *
 * @author Thomas Scharrenbach
 *
 * @version 0.0.2
 * @since 0.0.2
 */
public class CspNetworkParser {
	
	public CspNetwork parse(InputStream in) throws IOException {
		return parseInternal(new ANTLRInputStream(in));
	}

	private CspNetwork parseInternal(ANTLRInputStream in) {
		// Create Antlr lexer
		AntlrCspLexer lexer  = new AntlrCspLexer(in);

		// Get a list of matched tokens
		CommonTokenStream tokens = new CommonTokenStream(lexer);

		// Pass the tokens to the parser
		AntlrCspParser parser = new AntlrCspParser(tokens);

		// The entry point for parsing is the parser's output rule, since we
		// want to parse the whole document.
		CspConstraintsContext context = parser.cspConstraints();

		// Now we walk over the parse tree and the listener extracts the list of
		// constraints while traversing the tree.
		ParseTreeWalker walker = new ParseTreeWalker();
		AntlrCspNetworkLineListener listener = new AntlrCspNetworkLineListener();
		walker.walk(listener, context);

		return new CspNetwork(listener.getConstraints());
	}

	
}
