package qsr4sw.pyrcc8;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * <p>
 * Vocabulary for PyRCC8 output.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @since 0.0.2
 * @version 0.0.2
 * 
 */
public class PyRCC8Output {

	public static final String separator = "--------------------------------------";

	public static final String typeId = "Type ID:";

	public static final String visitedNodes = "Visited nodes:";

	public static final String visitedArcs = "Visited arcs:";

	public static final String checkedConstraints = "Checked constraints:";

	public static final String consistent = "Consistent";

	private PyRCC8Output() {

	}

}
