package qsr4sw.pyrcc8;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import qsr4sw.CspConstraint;
import qsr4sw.exec.CspExecutorOutputParser;
import qsr4sw.pyrcc8.AntlrPyRCC8OutputParser.PyRcc8OutputContext;

public class PyRCC8OutputParser implements CspExecutorOutputParser {

	//
	//
	//

	@Override
	public List<CspConstraint> parse(InputStream in) throws IOException {
		return parseInternal(new ANTLRInputStream(in));
	}

	private List<CspConstraint> parseInternal(ANTLRInputStream in) {
		// Create Antlr lexer
		AntlrPyRCC8OutputLexer lexer = new AntlrPyRCC8OutputLexer(in);

		// Get a list of matched tokens
		CommonTokenStream tokens = new CommonTokenStream(lexer);

		// Pass the tokens to the parser
		AntlrPyRCC8OutputParser parser = new AntlrPyRCC8OutputParser(tokens);

		// The entry point for parsing is the parser's output rule, since we
		// want to parse the whole document.
		PyRcc8OutputContext context = parser.pyRcc8Output();

		// Now we walk over the parse tree and the listener extracts the list of
		// constraints while traversing the tree.
		ParseTreeWalker walker = new ParseTreeWalker();
		AntlrPyRCC8OutputLineListener listener = new AntlrPyRCC8OutputLineListener();
		walker.walk(listener, context);

		return listener.getConstraints();
	}

	public static class Factory implements CspExecutorOutputParser.Factory {

		@Override
		public CspExecutorOutputParser createParser() {
			return new PyRCC8OutputParser();
		}
	}

}
