package qsr4sw.pyrcc8;

/*
 * #%L
 * qsr4sw-sandbox
 * %%
 * Copyright (C) 2014 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import qsr4sw.CspConstraint;
import qsr4sw.pyrcc8.AntlrPyRCC8OutputParser.Rcc8LineContext;
import qsr4sw.pyrcc8.AntlrPyRCC8OutputParser.Rcc8OpContext;

/**
 * <p>
 * This class extracts {@link CspConstraint} while parsing the output of a
 * PyRCC8 process.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.2
 * @since 0.0.2
 * 
 */
public class AntlrPyRCC8OutputLineListener extends
		AntlrPyRCC8OutputBaseListener {

	private final List<String> _currentOps;

	private final List<CspConstraint> _constraints;

	private CspConstraint _currentConstraint;

	public AntlrPyRCC8OutputLineListener() {
		_currentOps = new ArrayList<String>();
		_constraints = new ArrayList<CspConstraint>();
	}

	@Override
	public void enterRcc8Line(Rcc8LineContext ctx) {
		super.enterRcc8Line(ctx);
		_currentOps.clear();
		_currentConstraint = new CspConstraint();
	}

	@Override
	public void exitRcc8Line(Rcc8LineContext ctx) {
		super.exitRcc8Line(ctx);
		_currentConstraint.setLeftOp(Integer
				.parseInt(ctx.getChild(0).getText()));
		_currentConstraint.setRightOp(Integer.parseInt(ctx.getChild(1)
				.getText()));
		_currentConstraint.setOps(new ArrayList<String>(_currentOps));
		_constraints.add(_currentConstraint);
	}

	@Override
	public void enterRcc8Op(Rcc8OpContext ctx) {
		super.enterRcc8Op(ctx);
		_currentOps.add(ctx.getChild(1).getText());
	}

	/**
	 * 
	 * @return the list of extracted constraints.
	 */
	public List<CspConstraint> getConstraints() {
		return new ArrayList<CspConstraint>(_constraints);
	}

}
